jQuery(document).ready(function($) {
    let context = new AudioContext();

    let duration = 2;
    let interval = 0;
    let type = 'sine';
    let bass = 1.0;
    let volume = 0.5;
    let step = 0.05;
    let tone_span = $('#tone');
    let duration_span = $('#duration');
    let volume_span = $('#volume');
    let vawe_color;
    let chordType = 'default';
    let wave = "true";
    let analyser = context.createAnalyser();
    let WIDTH = 300;
    let HEIGHT = 300;
    let canvas = document.querySelector('.visualizer');
    let myCanvas = canvas.getContext("2d");
    let bufferLength = analyser.frequencyBinCount; 
    let dataArray = new Uint8Array(bufferLength);

    let octava = 4;
    let seq = {
        49: 16.35,
        50: 18.35,
        51: 20.6,
        52: 21.83,
        53: 24.5,
        54: 27.5,
        55: 30.87,
        81: 17.32,
        87: 19.45,
        69: 23.12,
        82: 25.96, 
        84: 29.14,
      };
    let octavas = {
        49: 1,
        50: 2,
        51: 3,
        52: 4,
        53: 5,
        54: 6,
        55: 7,
        56: 8,
    }

    tone_span[0].innerHTML = bass.toFixed(2);
    duration_span[0].innerHTML = duration.toFixed(2);
    volume_span[0].innerHTML = volume.toFixed(2) * 100 + "%";

    $('#colors').change(function(){
      vawe_color = $( "#colors option:selected" ).val();
    })

    $('#chord').change(function(){
      chordType = $( "#chord option:selected" ).val();
    })

    $('#wave').change(function(){
      wave = $( "#wave option:selected" ).val();
    })

    play(0, 0);

    function getOctavaFreq(originalFreq) {
        if(octava == 1)
            return originalFreq;
        let freq = originalFreq;
        for (let i = 1; i <= octava; i++) {
            freq = freq * 2;
        }
        return freq;
    }

    function playChord(frequency, time) {
        let chordFreq;

        if(chordType == "default") {
            chordFreq = [1];
        } else if(chordType == "up") {
            chordFreq = [2, 1.4, 1.25];
        } else {
            chordFreq = [1, 1.2, 1.4, 1.8, 2];
        }

        for (let i=0;i<chordFreq.length;i++) {
            play(frequency * chordFreq[i], time, 1 / chordFreq.length)
        }
    }

	function play(frequency, time, gain = 1) {
        let o = context.createOscillator();
        let filter = context.createBiquadFilter();
        let g = context.createGain();

        draw();

        o.type = type;
        g.gain.value = volume * gain;

        // filter.type = "notch";
        // filter.frequency.value = 1000;
        // filter.gain.value = 25;

        o.connect(g);
        g.connect(context.destination);
        g.connect(analyser); 
        g.gain.exponentialRampToValueAtTime(0.00001, context.currentTime + duration + time);
        o.frequency.value = frequency;
        o.start(0);
    }

	 $('input[type=radio]').click(function(event) {
	 	type = this.value;
	 });

	 $('#step').keydown(function(event) {
	 	step = this.value;
	 });

 	 $('button').click(function(event) {
	 	if(this.value == 'high') {
	 		bass = bass+step;
	 	} else if(this.value == 'low') {
	 		bass = bass-step;
	 	} else if(this.value == 'high_d') {
	 		duration = duration+0.5;
	 	} else if(this.value == 'low_d') {
	 		duration = duration-0.5;
	 	} else if(this.value == 'high_v') {
	 		volume = volume+0.1;
	 	} else if(this.value == 'low_v') {
	 		volume = volume-0.1;
	 	} else if(this.value == 'high_o') {
            octava = octava + 1;
            $('#octava').text(octava);
        }else if(this.value == 'low_o') {
            octava = octava - 1;
            $('#octava').text(octava);
        }
	 	volume_span[0].innerHTML = volume.toFixed(2) * 100 + "%";
	 	duration_span[0].innerHTML = duration.toFixed(2);
	 	tone_span[0].innerHTML = bass.toFixed(2);
	 });

     $('.piano li').click(function(e) {
        let char = $(this).attr('id').slice(4)
        keyDownHandler(char);
        
     });

     function keyDownHandler(keyCode) {
        if(seq[keyCode] !== undefined) {
            playChord(getOctavaFreq(seq[keyCode]) * bass, interval);
            // el[0].innerHTML += '<span style="color: ' +  color + ';" class="console-item">' +  e.key + '</span>';
            $("#key_" + keyCode).addClass('active');
            setTimeout( function(){ $("#key_" + keyCode).removeClass('active'); }, 200 );
        }
     }

    window.addEventListener('keydown', function(e) {
        if(e.ctrlKey) {
            if (octavas[e.keyCode] !== undefined)
                octava = octavas[e.keyCode];
                $('#octava').text(octava);
            return;
        }

       	let el = $('#console');
        let color = '#000';
        if(vawe_color == 'false') color = '#' + Math.floor(Math.random()*16777215).toString(16);

        keyDownHandler(e.keyCode)
        
        switch (e.keyCode) {
        	case 38:
        		 type = 'sine';
        		 $('#'+type).prop( "checked", true );
        		break;
        	case 40:
        		 type = 'square';
        		 $('#'+type).prop( "checked", true );
        		break;
        	case 37:
        		 type = 'triangle';
        		 $('#'+type).prop( "checked", true );
        		break;
        	case 39:
        		 type = 'sawtooth';
        		 $('#'+type).prop( "checked", true );
        		break;
        	case 13:
        		 el[0].innerHTML += '</br>';
        		break;
        	case 67:
        		 el[0].innerHTML = '';
                myCanvas.clearRect(0, 0, WIDTH, HEIGHT);
        		break;
        	default:
        		console.log(e.keyCode);
        		break;
       	}
    });

    analyser.fftSize = 512 * 2;
    myCanvas.clearRect(0, 0, WIDTH, HEIGHT);

    setInterval(function(){ 
        requestAnimationFrame(draw)   
    }, 10);

    function draw() {
        if(wave == 'true') {

            analyser.getByteTimeDomainData(dataArray);

            myCanvas.fillStyle = '#909e83';
            myCanvas.fillRect(0, 0, WIDTH, HEIGHT);
            myCanvas.lineWidth = 2;
            myCanvas.strokeStyle = '#101010';
            if(vawe_color == 'false') {
                myCanvas.strokeStyle = '#' + Math.floor(Math.random()*16777215).toString(16);
            } else if(vawe_color !== 'true') {
                myCanvas.strokeStyle = vawe_color;
            }
            myCanvas.beginPath();

            let sliceWidth = WIDTH * 1.0 / bufferLength ;
            let x = 0;

            for(let i = 0; i < bufferLength; i++) {

               let v = dataArray[i] / 128.0;
               let y = v * HEIGHT/6;

               if(i === 0) {
                 myCanvas.moveTo(x, y);
               } else {
                 myCanvas.lineTo(x, y);
               }

               x += sliceWidth;
             };

            myCanvas.lineTo(canvas.width, canvas.height/2);
            myCanvas.stroke();
        }
        
    };

});